<?php

namespace Drupal\field_constraints\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation of the field constraint plugin.
 *
 * @Annotation
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 */
class FieldConstraint extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The translated label of the field constraint displayed in UI.
   *
   * @var string
   */
  public $label;

  /**
   * An array of field types the constraint supports.
   *
   * @var array
   */
  public $field_types = [];

}
