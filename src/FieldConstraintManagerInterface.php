<?php

namespace Drupal\field_constraints;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides plugin manager of the field constraints.
 */
interface FieldConstraintManagerInterface extends PluginManagerInterface {

  /**
   * Returns plugin definitions that support specified field type.
   *
   * @param string $field_type
   *   The field type.
   *
   * @return array
   *   The list of plugin definitions keyed by the plugin ID.
   */
  public function getDefinitionsForFieldType(string $field_type): array;

}
