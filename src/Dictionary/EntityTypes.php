<?php

namespace Drupal\field_constraints\Dictionary;

/**
 * Dictionary class for the entity type IDs.
 */
final class EntityTypes {

  /**
   * The field config.
   */
  public const FIELD_CONFIG = 'field_config';

}
