<?php

namespace Drupal\field_constraints\Dictionary;

/**
 * Dictionary class for the field constraint settings on the field config.
 *
 * It defines keys used for the third-party settings of the field config.
 */
class FieldConfigSettingKeys {

  /**
   * The module used for the third-party settings.
   */
  public const MODULE = 'field_constraints';

  /**
   * The constraints key in the third-party module settings.
   */
  public const CONSTRAINTS = 'constraints';

}
