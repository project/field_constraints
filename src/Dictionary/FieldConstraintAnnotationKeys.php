<?php

namespace Drupal\field_constraints\Dictionary;

/**
 * Dictionary class of the keys on the field constraint plugin annotation.
 */
final class FieldConstraintAnnotationKeys {

  /**
   * The class name key.
   */
  public const CLASS_NAME = 'class';

  /**
   * The label key.
   */
  public const LABEL = 'label';

  /**
   * The field type list key.
   */
  public const FIELD_TYPES = 'field_types';

}
