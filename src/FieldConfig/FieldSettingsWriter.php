<?php

namespace Drupal\field_constraints\FieldConfig;

use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\Dictionary\FieldConfigSettingKeys;

/**
 * Provides settings writer for a field constraint of a field config.
 */
class FieldSettingsWriter implements FieldSettingsWriterInterface {

  /**
   * The module used for the third-party settings.
   */
  protected const MODULE = FieldConfigSettingKeys::MODULE;

  /**
   * The constraints key in the third-party module settings.
   */
  protected const CONSTRAINTS_KEY = FieldConfigSettingKeys::CONSTRAINTS;

  /**
   * The settings reader.
   *
   * @var \Drupal\field_constraints\FieldConfig\FieldSettingsReaderInterface
   */
  protected $reader;

  /**
   * Constructor.
   */
  public function __construct(
    FieldSettingsReaderInterface $reader
  ) {
    $this->reader = $reader;
  }

  /**
   * {@inheritdoc}
   */
  public function setConstraintConfig(
    FieldConfigInterface $field_config,
    string $plugin_id,
    array $plugin_config = []
  ): void {
    $constraints = $this->reader->getConstraintConfigs($field_config);
    $constraints[$plugin_id] = $plugin_config;
    $this->setConstraintConfigs($field_config, $constraints);
  }

  /**
   * {@inheritdoc}
   */
  public function setConstraintConfigs(
    FieldConfigInterface $field_config,
    array $plugin_configs
  ): void {
    if (!empty($plugin_configs)) {
      $field_config->setThirdPartySetting(
        static::MODULE,
        static::CONSTRAINTS_KEY,
        $plugin_configs
      );
    }
    else {
      $field_config->unsetThirdPartySetting(
        static::MODULE,
        static::CONSTRAINTS_KEY
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeConstraint(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): void {
    $constraints = $this->reader->getConstraintConfigs($field_config);
    unset($constraints[$plugin_id]);
    $this->setConstraintConfigs($field_config, $constraints);
  }

}
