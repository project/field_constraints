<?php

namespace Drupal\field_constraints\FieldConfig;

use Drupal\field\FieldConfigInterface;

/**
 * Provides field constraints factory for the field config constraints.
 */
interface FieldConfigConstraintsFactoryInterface {

  /**
   * Creates field constraint instances for the specified field config.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   *
   * @return \Drupal\field_constraints\FieldConstraintInterface[]
   *   The list of field constraints configured for the field, keyed by
   *   the field constraint plugin ID.
   */
  public function createFieldConfigConstraints(
    FieldConfigInterface $field_config
  ): array;

}
