<?php

namespace Drupal\field_constraints\FieldConfig;

use Drupal\field\FieldConfigInterface;

/**
 * Provides settings reader for a field constraint of a field config.
 */
interface FieldSettingsReaderInterface {

  /**
   * Returns field constraint configurations found in the field config.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config to read the configurations from.
   *
   * @return array
   *   The list of constraint configurations keyed by the plugin ID.
   */
  public function getConstraintConfigs(
    FieldConfigInterface $field_config
  ): array;

  /**
   * Checks if the specified field constraint exists on the field config.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   *
   * @return bool
   *   TRUE if the field constraint is enabled on the field.
   */
  public function constraintExists(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): bool;

  /**
   * Returns field constraint configuration found on the field config.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config to read the settings from.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   *
   * @return array
   *   The field constraint configuration. Empty array is returned if the plugin
   *   doesn't exist on the field config.
   *
   * @see constraintExists()
   */
  public function getConstraintConfig(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): array;

}
