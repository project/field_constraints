<?php

namespace Drupal\field_constraints\FieldConfig;

use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\FieldConstraintManagerInterface;

/**
 * Provides field constraints factory for the field config constraints.
 */
class FieldConfigConstraintsFactory implements FieldConfigConstraintsFactoryInterface {

  /**
   * The settings reader.
   *
   * @var \Drupal\field_constraints\FieldConfig\FieldSettingsReaderInterface
   */
  protected $settingsReader;

  /**
   * The field constraints plugin manager.
   *
   * @var \Drupal\field_constraints\FieldConstraintManagerInterface
   */
  protected $fieldConstraintManager;

  /**
   * A constructor.
   */
  public function __construct(
    FieldSettingsReaderInterface $settings_reader,
    FieldConstraintManagerInterface $field_constraint_manager
  ) {
    $this->settingsReader = $settings_reader;
    $this->fieldConstraintManager = $field_constraint_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function createFieldConfigConstraints(
    FieldConfigInterface $field_config
  ): array {
    $plugin_configs = $this->settingsReader
      ->getConstraintConfigs($field_config);

    $result = [];
    foreach ($plugin_configs as $plugin_id => $plugin_config) {
      /** @var \Drupal\field_constraints\FieldConstraintInterface $plugin */
      $plugin = $this->fieldConstraintManager
        ->createInstance($plugin_id, $plugin_config);
      $result[$plugin_id] = $plugin;
    }
    return $result;
  }

}
