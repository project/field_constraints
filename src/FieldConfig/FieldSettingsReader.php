<?php

namespace Drupal\field_constraints\FieldConfig;

use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\Dictionary\FieldConfigSettingKeys;

/**
 * Provides settings reader for a field constraint of a field config.
 */
class FieldSettingsReader implements FieldSettingsReaderInterface {

  /**
   * The module used for the third-party settings.
   */
  protected const MODULE = FieldConfigSettingKeys::MODULE;

  /**
   * The constraints key in the third-party module settings.
   */
  protected const CONSTRAINTS_KEY = FieldConfigSettingKeys::CONSTRAINTS;

  /**
   * {@inheritdoc}
   */
  public function getConstraintConfigs(
    FieldConfigInterface $field_config
  ): array {
    return $field_config
      ->getThirdPartySetting(static::MODULE, static::CONSTRAINTS_KEY, []);
  }

  /**
   * {@inheritdoc}
   */
  public function constraintExists(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): bool {
    $configs = $this->getConstraintConfigs($field_config);
    return isset($configs[$plugin_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraintConfig(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): array {
    $configs = $this->getConstraintConfigs($field_config);
    return $configs[$plugin_id] ?? [];
  }

}
