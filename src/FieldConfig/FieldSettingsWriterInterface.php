<?php

namespace Drupal\field_constraints\FieldConfig;

use Drupal\field\FieldConfigInterface;

/**
 * Provides settings writer for a field constraint of a field config.
 */
interface FieldSettingsWriterInterface {

  /**
   * Sets the field constraint configuration on the field config.
   *
   * This also enabled the constraint on the field. The field config is not
   * saved in this method, the caller should do it separately.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   * @param array $plugin_config
   *   The field constraint plugin configuration.
   */
  public function setConstraintConfig(
    FieldConfigInterface $field_config,
    string $plugin_id,
    array $plugin_config = []
  ): void;

  /**
   * Sets configuration of all the field constraints on the field config.
   *
   * The field config is not saved in this method, the caller should do it
   * separately.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param array $plugin_configs
   *   The field constraints configuration keyed by plugin ID.
   */
  public function setConstraintConfigs(
    FieldConfigInterface $field_config,
    array $plugin_configs
  ): void;

  /**
   * Removes field constraint from the field config.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config to remove it from.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   */
  public function removeConstraint(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): void;

}
