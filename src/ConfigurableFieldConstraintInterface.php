<?php

namespace Drupal\field_constraints;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface of a configurable field constraint.
 *
 * It extends the basic interface in order to provide a configuration form.
 *
 * @see \Drupal\field_constraints\FieldConstraintInterface
 */
interface ConfigurableFieldConstraintInterface extends FieldConstraintInterface, ConfigurableInterface, PluginFormInterface {

}
