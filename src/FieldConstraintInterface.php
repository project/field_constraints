<?php

namespace Drupal\field_constraints;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Interface of a field constraint.
 *
 * The interface instance is responsible for exposing compatible validation
 * constraints to the field configs of supported types and applying them to the
 * bundle fields. It could also provide configuration form for the validation
 * constraint.
 *
 * @see \Drupal\field_constraints\ConfigurableFieldConstraintInterface
 */
interface FieldConstraintInterface extends PluginInspectionInterface {

  /**
   * Applies the validation constraint to the field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   *   The field to apply the constraints to.
   */
  public function apply(FieldDefinitionInterface $field): void;

}
