<?php

namespace Drupal\field_constraints;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides field constraint that replaces missing/broken plugins.
 *
 * It isn't registered as a plugin itself to exclude it from all the listings.
 */
class BrokenFieldConstraint extends FieldConstraintBase {

  /**
   * {@inheritdoc}
   */
  public function apply(FieldDefinitionInterface $field): void {
    // Broken handler doesn't add anything to the field.
  }

}
