<?php

namespace Drupal\field_constraints\Entity;

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Interface of an entity bundle field alterer.
 */
interface EntityBundleFieldAltererInterface {

  /**
   * Alters bundle fields of the entity.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $fields
   *   The array of bundle field definitions.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param string $bundle
   *   The bundle.
   *
   * @see hook_entity_bundle_field_info_alter()
   */
  public function alterBundleFields(
    array &$fields,
    EntityTypeInterface $entity_type,
    string $bundle
  ): void;

}
