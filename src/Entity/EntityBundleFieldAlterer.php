<?php

namespace Drupal\field_constraints\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\field_constraints\FieldConstraintsFactoryInterface;

/**
 * Alters field configs of the entity to add constraints to them.
 */
class EntityBundleFieldAlterer implements EntityBundleFieldAltererInterface {

  /**
   * The field constraints factory.
   *
   * @var \Drupal\field_constraints\FieldConstraintsFactoryInterface
   */
  protected $constraintsFactory;

  /**
   * A constructor.
   */
  public function __construct(
    FieldConstraintsFactoryInterface $constraints_factory
  ) {
    $this->constraintsFactory = $constraints_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function alterBundleFields(
    array &$fields,
    EntityTypeInterface $entity_type,
    string $bundle
  ): void {
    foreach ($fields as $field) {
      $constraints = $this->constraintsFactory->createFieldConstraints($field);
      foreach ($constraints as $constraint) {
        $constraint->apply($field);
      }
    }
  }

}
