<?php

namespace Drupal\field_constraints;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides field constraints factory for the field.
 */
interface FieldConstraintsFactoryInterface {

  /**
   * Creates field constraints of the specified field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition to create the field constraints for.
   *
   * @return \Drupal\field_constraints\FieldConstraintInterface[]
   *   The list of field constraints configured for the field, keyed by
   *   the field constraint plugin ID.
   */
  public function createFieldConstraints(
    FieldDefinitionInterface $field_definition
  ): array;

}
