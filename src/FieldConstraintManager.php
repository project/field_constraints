<?php

namespace Drupal\field_constraints;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\field_constraints\Annotation\FieldConstraint;
use Drupal\field_constraints\Dictionary\FieldConstraintAnnotationKeys;

/**
 * Provides plugin manager of the field constraints.
 */
class FieldConstraintManager extends DefaultPluginManager implements FieldConstraintManagerInterface {

  /**
   * The plugin sub-folder in the module.
   */
  public const SUBDIR = 'Plugin/field_constraints/Field';

  /**
   * The plugin annotation class name.
   */
  public const PLUGIN_ANNOTATION_NAME = FieldConstraint::class;

  /**
   * The cache key.
   */
  public const CACHE_KEY = 'field_constraints';

  /**
   * The plugin interface.
   */
  public const PLUGIN_INTERFACE = FieldConstraintInterface::class;

  /**
   * The field type list key in the plugin definition.
   */
  protected const FIELD_TYPES_KEY = FieldConstraintAnnotationKeys::FIELD_TYPES;

  /**
   * The hook name for the plugin definition alter.
   */
  public const ALTER_HOOK = 'field_constraints';

  /**
   * A constructor.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      static::SUBDIR,
      $namespaces,
      $module_handler,
      static::PLUGIN_INTERFACE,
      static::PLUGIN_ANNOTATION_NAME
    );

    $this->setCacheBackend($cache_backend, static::CACHE_KEY);
    $this->alterInfo(static::ALTER_HOOK);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsForFieldType(string $field_type): array {
    $result = [];

    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      $field_types = $definition[static::FIELD_TYPES_KEY] ?? [];
      if (in_array($field_type, $field_types, TRUE)) {
        $result[$plugin_id] = $definition;
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    try {
      return parent::createInstance($plugin_id, $configuration);
    }
    catch (PluginNotFoundException $e) {
      return new BrokenFieldConstraint($configuration, $plugin_id, []);
    }
  }

}
