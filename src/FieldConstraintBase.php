<?php

namespace Drupal\field_constraints;

use Drupal\Core\Plugin\PluginBase;

/**
 * Provides base class for the field constraint plugin.
 */
abstract class FieldConstraintBase extends PluginBase implements FieldConstraintInterface {

}
