<?php

namespace Drupal\field_constraints;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\FieldConfig\FieldConfigConstraintsFactoryInterface;

/**
 * Provides field constraints factory for the field.
 */
class FieldConstraintsFactory implements FieldConstraintsFactoryInterface {

  /**
   * The constraints factory for the field configs.
   *
   * @var \Drupal\field_constraints\FieldConfig\FieldConfigConstraintsFactoryInterface
   */
  protected $fieldConfigConstraintsFactory;

  /**
   * A constructor.
   */
  public function __construct(
    FieldConfigConstraintsFactoryInterface $field_config_constraints_factory
  ) {
    $this->fieldConfigConstraintsFactory = $field_config_constraints_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function createFieldConstraints(
    FieldDefinitionInterface $field_definition
  ): array {
    if ($field_definition instanceof FieldConfigInterface) {
      return $this->fieldConfigConstraintsFactory
        ->createFieldConfigConstraints($field_definition);
    }

    return [];
  }

}
