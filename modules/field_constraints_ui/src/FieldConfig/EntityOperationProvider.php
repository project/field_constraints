<?php

namespace Drupal\field_constraints_ui\FieldConfig;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field_constraints_ui\Entity\EntityOperationProviderInterface;

/**
 * Entity operation provider for a field config.
 */
class EntityOperationProvider implements EntityOperationProviderInterface {

  use StringTranslationTrait;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The field constraint permission provider.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\PermissionProviderInterface
   */
  protected $permissionProvider;

  /**
   * The field constraint URL builder.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\ConstraintUrlBuilderInterface
   */
  protected $urlBuilder;

  /**
   * A constructor.
   */
  public function __construct(
    AccountInterface $current_user,
    PermissionProviderInterface $permission_provider,
    ConstraintUrlBuilderInterface $url_builder
  ) {
    $this->currentUser = $current_user;
    $this->permissionProvider = $permission_provider;
    $this->urlBuilder = $url_builder;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityOperations(EntityInterface $field_config): ?array {
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    if (!$field_config->access('update')) {
      return NULL;
    }

    $target_entity_type_id = $field_config->getTargetEntityTypeId();
    $permission = $this->permissionProvider
      ->getEntityTypePermission($target_entity_type_id);
    if (!$this->currentUser->hasPermission($permission)) {
      return NULL;
    }

    return [
      'field-constraints' => [
        'title' => $this->t('Constraints'),
        'weight' => 20,
        'url' => $this->urlBuilder->buildCollectionUrl($field_config),
      ],
    ];
  }

}
