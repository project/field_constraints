<?php

namespace Drupal\field_constraints_ui\FieldConfig;

/**
 * Provides dynamic permissions of the module.
 */
interface PermissionProviderInterface {

  /**
   * Returns the administration permission name for the specified entity type.
   *
   * The permission is required to administer the field constraints of the
   * entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return string
   *   The permission name.
   */
  public function getEntityTypePermission(string $entity_type_id): string;

  /**
   * Returns an array of permission definitions for the field constraints.
   *
   * A separate permission is defined for every fieldable entity type in the
   * system. The permission is required to administer the field constraints of
   * the entity type.
   *
   * @return array
   *   The array of permission definitions similar to the one provided in .yml
   *   files, keyed by the permission name.
   */
  public function getPermissionDefinitions();

}
