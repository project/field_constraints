<?php

namespace Drupal\field_constraints_ui\FieldConfig;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides dynamic permissions of the module.
 */
class PermissionProvider implements PermissionProviderInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypePermission(string $entity_type_id): string {
    return 'administer ' . $entity_type_id . ' field constraints';
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissionDefinitions() {
    $permissions = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type->get('field_ui_base_route')) {
        // Create a permission for each fieldable entity to manage the field
        // constraints.
        $permission_name = $this->getEntityTypePermission($entity_type_id);
        $permissions[$permission_name] = [
          'title' => $this->t(
            '%entity_label: Administer field constraints',
            ['%entity_label' => $entity_type->getLabel()]
          ),
          'restrict access' => TRUE,
        ];
      }
    }

    return $permissions;
  }

}
