<?php

namespace Drupal\field_constraints_ui\FieldConfig;

use Drupal\field\FieldConfigInterface;

/**
 * Provides route names and parameters for various field constraint operations.
 */
interface RouteProviderInterface {

  /**
   * Returns route name for a listing of constraints for the field config.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return string
   *   The route name.
   */
  public function getCollectionRouteName(string $entity_type_id): string;

  /**
   * Returns route name for the add constraint form of the field config.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return string
   *   The route name.
   */
  public function getAddFormRouteName(string $entity_type_id): string;

  /**
   * Returns route name for the edit constraint form of the field config.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return string
   *   The route name.
   */
  public function getEditFormRouteName(string $entity_type_id): string;

  /**
   * Returns route name for the remove constraint form of the field config.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return string
   *   The route name.
   */
  public function getRemoveFormRouteName(string $entity_type_id): string;

  /**
   * Returns route parameters for all the routes provided by the class.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string|null $plugin_id
   *   The field constraint plugin ID or NULL if it's not necessary for the
   *   route.
   *
   * @return array
   *   The route parameters.
   */
  public function getRouteParameters(
    FieldConfigInterface $field_config,
    string $plugin_id = NULL
  ): array;

}
