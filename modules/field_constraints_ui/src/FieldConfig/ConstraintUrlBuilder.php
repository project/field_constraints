<?php

namespace Drupal\field_constraints_ui\FieldConfig;

use Drupal\Core\Url;
use Drupal\field\FieldConfigInterface;

/**
 * Builds URLs for various field constraint operations.
 */
class ConstraintUrlBuilder implements ConstraintUrlBuilderInterface {

  /**
   * The route provider.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * A constructor.
   */
  public function __construct(
    RouteProviderInterface $route_provider
  ) {
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public function buildCollectionUrl(FieldConfigInterface $field_config): Url {
    $target_entity_type_id = $field_config->getTargetEntityTypeId();
    $route_name = $this->routeProvider
      ->getCollectionRouteName($target_entity_type_id);
    $route_parameters = $this->routeProvider->getRouteParameters($field_config);
    return Url::fromRoute($route_name, $route_parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEditUrl(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): Url {
    $target_entity_type_id = $field_config->getTargetEntityTypeId();
    $route_name = $this->routeProvider
      ->getEditFormRouteName($target_entity_type_id);
    $route_parameters = $this->routeProvider
      ->getRouteParameters($field_config, $plugin_id);
    return Url::fromRoute($route_name, $route_parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function buildRemoveUrl(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): Url {
    $target_entity_type_id = $field_config->getTargetEntityTypeId();
    $route_name = $this->routeProvider
      ->getRemoveFormRouteName($target_entity_type_id);
    $route_parameters = $this->routeProvider
      ->getRouteParameters($field_config, $plugin_id);
    return Url::fromRoute($route_name, $route_parameters);
  }

}
