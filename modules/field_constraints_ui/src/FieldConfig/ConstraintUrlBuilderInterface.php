<?php

namespace Drupal\field_constraints_ui\FieldConfig;

use Drupal\Core\Url;
use Drupal\field\FieldConfigInterface;

/**
 * Builds URLs for various field constraint operations.
 */
interface ConstraintUrlBuilderInterface {

  /**
   * Builds URL of the field constraint listing.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   *
   * @return \Drupal\Core\Url
   *   The URL.
   */
  public function buildCollectionUrl(FieldConfigInterface $field_config): Url;

  /**
   * Builds URL of the field constraint edit form.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   *
   * @return \Drupal\Core\Url
   *   The URL.
   */
  public function buildEditUrl(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): Url;

  /**
   * Builds URL of the field constraint remove form.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   *
   * @return \Drupal\Core\Url
   *   The URL.
   */
  public function buildRemoveUrl(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): Url;

}
