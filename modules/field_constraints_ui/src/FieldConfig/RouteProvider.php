<?php

namespace Drupal\field_constraints_ui\FieldConfig;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Provides route names and parameters for various field constraint operations.
 */
class RouteProvider implements RouteProviderInterface {

  /**
   * The prefix added to all the route names.
   */
  public const ROUTE_NAMESPACE = 'field_constraints_ui';

  /**
   * The listing route type.
   */
  public const COLLECTION_ROUTE_TYPE = 'collection';

  /**
   * The add form route type.
   */
  public const ADD_FORM_ROUTE_TYPE = 'add';

  /**
   * The edit form route type.
   */
  public const EDIT_FORM_ROUTE_TYPE = 'edit';

  /**
   * The remove form route type.
   */
  public const REMOVE_FORM_ROUTE_TYPE = 'remove';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Returns route name for the specified entity type ID and route type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $route_type
   *   The route type. That's the suffix added to the route name.
   *
   * @return string
   *   The route name.
   */
  protected function getRouteName(
    string $entity_type_id,
    string $route_type
  ): string {
    $namespace = static::ROUTE_NAMESPACE;
    return "{$namespace}.{$entity_type_id}.{$route_type}";
  }

  /**
   * {@inheritdoc}
   */
  public function getCollectionRouteName(string $entity_type_id): string {
    return $this->getRouteName($entity_type_id, static::COLLECTION_ROUTE_TYPE);
  }

  /**
   * {@inheritdoc}
   */
  public function getAddFormRouteName(string $entity_type_id): string {
    return $this->getRouteName($entity_type_id, static::ADD_FORM_ROUTE_TYPE);
  }

  /**
   * {@inheritdoc}
   */
  public function getEditFormRouteName(string $entity_type_id): string {
    return $this->getRouteName($entity_type_id, static::EDIT_FORM_ROUTE_TYPE);
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoveFormRouteName(string $entity_type_id): string {
    return $this->getRouteName($entity_type_id, static::REMOVE_FORM_ROUTE_TYPE);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(
    FieldConfigInterface $field_config,
    string $plugin_id = NULL
  ): array {
    $route_parameters = [
      'field_config' => $field_config->id(),
    ];

    $target_entity_type_id = $field_config->getTargetEntityTypeId();
    $target_entity_type = $this->entityTypeManager
      ->getDefinition($target_entity_type_id);
    $target_bundle_entity_type_id = $target_entity_type->getBundleEntityType();
    if ($target_bundle_entity_type_id) {
      $target_bundle_id = $field_config->getTargetBundle();
      $route_parameters[$target_bundle_entity_type_id] = $target_bundle_id;
    }

    if ($plugin_id !== NULL) {
      $route_parameters['plugin_id'] = $plugin_id;
    }

    return $route_parameters;
  }

}
