<?php

namespace Drupal\field_constraints_ui\Entity;

use Drupal\Core\Entity\EntityInterface;

/**
 * The entity operations provider interface.
 */
interface EntityOperationProviderInterface {

  /**
   * Provides entity operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to provide the operations for.
   *
   * @return array|null
   *   The list of operations or NULL if there are no operations to add.
   *
   * @see hook_entity_operation()
   */
  public function getEntityOperations(EntityInterface $entity): ?array;

}
