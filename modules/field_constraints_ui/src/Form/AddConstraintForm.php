<?php

namespace Drupal\field_constraints_ui\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\ConfigurableFieldConstraintInterface;
use Drupal\field_constraints\Dictionary\FieldConstraintAnnotationKeys;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The form that handles constraint addition to the field config.
 */
class AddConstraintForm extends FieldConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public const FORM_ID = 'field_constraints_add_constraint_form';

  /**
   * The label key on the field constraint plugin definition.
   */
  protected const LABEL_DEFINITION_KEY = FieldConstraintAnnotationKeys::LABEL;

  /**
   * The class name key on the field constraint plugin definition.
   */
  protected const CLASS_DEFINITION_KEY = FieldConstraintAnnotationKeys::CLASS_NAME;

  /**
   * The field constraint plugin manager.
   *
   * @var \Drupal\field_constraints\FieldConstraintManagerInterface
   */
  protected $fieldConstraintManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field constraint settings reader.
   *
   * @var \Drupal\field_constraints\FieldConfig\FieldSettingsReaderInterface
   */
  protected $settingsReader;

  /**
   * The field constraint settings writer.
   *
   * @var \Drupal\field_constraints\FieldConfig\FieldSettingsWriterInterface
   */
  protected $settingsWriter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var static $instance */
    $instance = parent::create($container);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->fieldConstraintManager = $container->get(
      'plugin.manager.field_constraints_manager'
    );
    $instance->settingsReader = $container->get(
      'field_constraints.field_config.settings_reader'
    );
    $instance->settingsWriter = $container->get(
      'field_constraints.field_config.settings_writer'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    FieldConfigInterface $field_config = NULL
  ): array {
    if ($field_config === NULL) {
      throw new \InvalidArgumentException(sprintf(
        'The field config argument is required.'
      ));
    }
    $this->setFieldConfig($field_config, $form_state);

    $definitions = $this->fieldConstraintManager
      ->getDefinitionsForFieldType($field_config->getType());
    $existing_constraints = $this->settingsReader
      ->getConstraintConfigs($field_config);
    $definitions = array_diff_key($definitions, $existing_constraints);
    if (empty($definitions)) {
      if (!empty($existing_constraints)) {
        $message = $this->t('All the available constraints have already been added.');
      }
      else {
        $message = $this->t('No constraints available for the field type.');
      }

      $form['message'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [$message],
        ],
        '#status_headings' => [
          'warning' => $this->t('Warning message'),
        ],
      ];
      return $form;
    }

    $constraint_options = [];
    foreach ($definitions as $plugin_id => $definition) {
      $label = $definition[static::LABEL_DEFINITION_KEY] ?? NULL;
      if ($label === NULL) {
        continue;
      }

      $constraint_options[$plugin_id] = $label;
    }

    $form['plugin_id'] = [
      '#required' => TRUE,
      '#type' => 'select',
      '#title' => $this->t('Constraint type'),
      '#options' => $constraint_options,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $form_state->getValue('plugin_id');
    $field_config = $this->getFieldConfig($form_state);
    if ($this->settingsReader->constraintExists($field_config, $plugin_id)) {
      $form_state->setErrorByName(
        'plugin_id',
        $this->t('The constraint already exists on the field.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field_config = $this->getFieldConfig($form_state);

    $plugin_id = $form_state->getValue('plugin_id');
    $plugin_definition = $this->fieldConstraintManager
      ->getDefinition($plugin_id);

    $class = $plugin_definition[static::CLASS_DEFINITION_KEY];
    if (is_subclass_of($class, ConfigurableFieldConstraintInterface::class)) {
      $this->setRedirectToPluginConfig($field_config, $form_state, $plugin_id);
    }
    else {
      $this->settingsWriter->setConstraintConfig($field_config, $plugin_id);
      $field_config->save();

      $this->messenger()
        ->addStatus($this->t('The constraint was added.'));
      $this->setRedirectToConstraintCollection($field_config, $form_state);
    }
  }

  /**
   * Sets the form redirect to the field constraint configuration URL.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $plugin_id
   *   The plugin ID.
   */
  protected function setRedirectToPluginConfig(
    FieldConfigInterface $field_config,
    FormStateInterface $form_state,
    string $plugin_id
  ): void {
    $url = $this->constraintUrlBuilder->buildEditUrl($field_config, $plugin_id);
    $form_state->setRedirectUrl($url);
  }

}
