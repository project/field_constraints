<?php

namespace Drupal\field_constraints_ui\Form;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\Dictionary\FieldConstraintAnnotationKeys;

/**
 * The form that handles constraint removal from the field config.
 */
class DeleteConstraintForm extends FieldConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public const FORM_ID = 'field_constraints_delete_constraint_form';

  /**
   * The form state key that holds the field constraint plugin ID.
   */
  public const PLUGIN_ID_STATE_KEY = 'field_constraint_plugin';

  /**
   * The label key on the field constraint plugin definition.
   */
  protected const LABEL_DEFINITION_KEY = FieldConstraintAnnotationKeys::LABEL;

  /**
   * Returns the form route title.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The title.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function title(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): MarkupInterface {
    /** @var \Drupal\field_constraints\FieldConstraintManagerInterface $plugin_manager */
    $plugin_manager = \Drupal::service(
      'plugin.manager.field_constraints_manager'
    );
    $definition = $plugin_manager->getDefinition($plugin_id);
    $label = $definition[static::LABEL_DEFINITION_KEY]
      ?? t('Broken/missing handler');

    return t('Are you sure to remove the %plugin constraint from the @field field?', [
      '%plugin' => $label,
      '@field' => $field_config->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    FieldConfigInterface $field_config = NULL,
    string $plugin_id = NULL
  ) {
    $this->setFieldConfig($field_config, $form_state);
    $this->setConstraintPluginId($plugin_id, $form_state);

    $form['#theme'] = 'confirm_form';
    $form['#attributes']['class'][] = 'confirmation';

    $form['#title'] = static::title($field_config, $plugin_id);
    $form['description'] = [
      '#markup' => $this->t('This action cannot be undone.'),
    ];
    $form['plugin_' . $plugin_id] = [
      '#type' => 'hidden',
      '#value' => 1,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#button_type' => 'primary',
    ];

    $url = $this->constraintUrlBuilder
      ->buildCollectionUrl($field_config);
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => $url,
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Sets the field constraint plugin ID to the form state.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @see getConstraintPluginId()
   */
  protected function setConstraintPluginId(
    string $plugin_id,
    FormStateInterface $form_state
  ): void {
    $form_state->set(static::PLUGIN_ID_STATE_KEY, $plugin_id);
  }

  /**
   * Returns the field constraint plugin ID found in the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   The plugin ID.
   *
   * @see setConstraintPluginId()
   */
  protected function getConstraintPluginId(
    FormStateInterface $form_state
  ): string {
    return $form_state->get(static::PLUGIN_ID_STATE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field_config = $this->getFieldConfig($form_state);
    $plugin_id = $this->getConstraintPluginId($form_state);
    $this->settingsWriter->removeConstraint($field_config, $plugin_id);
    $field_config->save();

    $this->messenger()
      ->addStatus($this->t('The constraint was removed.'));

    $this->setRedirectToConstraintCollection($field_config, $form_state);
  }

}
