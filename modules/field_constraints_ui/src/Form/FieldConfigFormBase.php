<?php

namespace Drupal\field_constraints_ui\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\Dictionary\EntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for the field constraint forms of the field config.
 */
abstract class FieldConfigFormBase extends FormBase {

  /**
   * The form state key that holds the field config ID.
   */
  public const FIELD_CONFIG_ID_STATE_KEY = 'field_config';

  /**
   * The field config entity types.
   */
  protected const FIELD_CONFIG_ENTITY_TYPE = EntityTypes::FIELD_CONFIG;

  /**
   * The field constraint plugin manager.
   *
   * @var \Drupal\field_constraints\FieldConstraintManagerInterface
   */
  protected $fieldConstraintManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field constraint settings reader.
   *
   * @var \Drupal\field_constraints\FieldConfig\FieldSettingsReaderInterface
   */
  protected $settingsReader;

  /**
   * The field constraint settings writer.
   *
   * @var \Drupal\field_constraints\FieldConfig\FieldSettingsWriterInterface
   */
  protected $settingsWriter;

  /**
   * The field constraint URL builder.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\ConstraintUrlBuilderInterface
   */
  protected $constraintUrlBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var static $instance */
    $instance = parent::create($container);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->fieldConstraintManager = $container->get(
      'plugin.manager.field_constraints_manager'
    );
    $instance->settingsReader = $container->get(
      'field_constraints.field_config.settings_reader'
    );
    $instance->settingsWriter = $container->get(
      'field_constraints.field_config.settings_writer'
    );
    $instance->constraintUrlBuilder = $container->get(
      'field_constraints_ui.field_config.constraint_url_builder'
    );

    return $instance;
  }

  /**
   * Sets the field config ID to the form state.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @see getFieldConfig()
   */
  protected function setFieldConfig(
    FieldConfigInterface $field_config,
    FormStateInterface $form_state
  ): void {
    $form_state->set(static::FIELD_CONFIG_ID_STATE_KEY, $field_config->id());
  }

  /**
   * Returns the field config saved in the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\field\FieldConfigInterface
   *   The field config.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see setFieldConfig()
   */
  protected function getFieldConfig(
    FormStateInterface $form_state
  ): FieldConfigInterface {
    $field_config_id = $form_state->get(static::FIELD_CONFIG_ID_STATE_KEY);
    if ($field_config_id === NULL) {
      throw new \LogicException('Field config ID is missing.');
    }

    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = $this->entityTypeManager
      ->getStorage(static::FIELD_CONFIG_ENTITY_TYPE)
      ->load($field_config_id);
    return $field_config;
  }

  /**
   * Sets the form redirect to the field constraint collection URL.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function setRedirectToConstraintCollection(
    FieldConfigInterface $field_config,
    FormStateInterface $form_state
  ): void {
    $url = $this->constraintUrlBuilder->buildCollectionUrl($field_config);
    $form_state->setRedirectUrl($url);
  }

}
