<?php

namespace Drupal\field_constraints_ui\Form;

use Drupal\Core\Form\FormBase as CoreFormBase;

/**
 * Provides base form class.
 */
abstract class FormBase extends CoreFormBase {

  /**
   * The form ID.
   *
   * Must be specified in child classes.
   */
  protected const FORM_ID = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::FORM_ID;
  }

}
