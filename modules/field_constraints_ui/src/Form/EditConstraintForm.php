<?php

namespace Drupal\field_constraints_ui\Form;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\ConfigurableFieldConstraintInterface;
use Drupal\field_constraints\Dictionary\FieldConstraintAnnotationKeys;

/**
 * The form that handles constraint configuration on the field config.
 *
 * It is responsible for configuring both new and exsiting field constraint.
 */
class EditConstraintForm extends FieldConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public const FORM_ID = 'field_constraints_add_constraint_form';

  /**
   * The form state key that holds the field constraint plugin ID.
   */
  public const PLUGIN_ID_STATE_KEY = 'field_constraint_plugin';

  /**
   * The label key on the field constraint plugin definition.
   */
  protected const LABEL_DEFINITION_KEY = FieldConstraintAnnotationKeys::LABEL;

  /**
   * The cached field constraint plugin instance.
   *
   * @var \Drupal\field_constraints\ConfigurableFieldConstraintInterface|null
   */
  protected $plugin;

  /**
   * Returns the form route title.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The title.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function title(
    FieldConfigInterface $field_config,
    string $plugin_id
  ): MarkupInterface {
    /** @var \Drupal\field_constraints\FieldConstraintManagerInterface $plugin_manager */
    $plugin_manager = \Drupal::service(
      'plugin.manager.field_constraints_manager'
    );
    $definition = $plugin_manager->getDefinition($plugin_id);
    $label = $definition[static::LABEL_DEFINITION_KEY]
      ?? t('Broken/missing handler');

    return t('Configure the %plugin constraint of the @field field', [
      '%plugin' => $label,
      '@field' => $field_config->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    FieldConfigInterface $field_config = NULL,
    string $plugin_id = NULL
  ) {
    $this->setFieldConfig($field_config, $form_state);
    $this->setConstraintPluginId($plugin_id, $form_state);

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    $plugin = $this->getConstraintPlugin($form_state);
    return $plugin->buildConfigurationForm($form, $form_state);
  }

  /**
   * Sets the field constraint plugin ID to the form state.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @see getConstraintPluginId()
   */
  protected function setConstraintPluginId(
    string $plugin_id,
    FormStateInterface $form_state
  ): void {
    $form_state->set(static::PLUGIN_ID_STATE_KEY, $plugin_id);
  }

  /**
   * Returns the field constraint plugin ID found in the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   The plugin ID.
   *
   * @see setConstraintPluginId()
   */
  protected function getConstraintPluginId(
    FormStateInterface $form_state
  ): string {
    return $form_state->get(static::PLUGIN_ID_STATE_KEY);
  }

  /**
   * Returns the field constraint plugin of the ID found in the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\field_constraints\ConfigurableFieldConstraintInterface
   *   The plugin instance.
   */
  protected function getConstraintPlugin(
    FormStateInterface $form_state
  ): ConfigurableFieldConstraintInterface {
    if (!isset($this->plugin)) {
      $this->plugin = $this->createConstraintPlugin($form_state);
    }

    return $this->plugin;
  }

  /**
   * Creates the field constraint plugin of the ID found in the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\field_constraints\ConfigurableFieldConstraintInterface
   *   The plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createConstraintPlugin(
    FormStateInterface $form_state
  ): ConfigurableFieldConstraintInterface {
    $field_config = $this->getFieldConfig($form_state);
    $plugin_id = $this->getConstraintPluginId($form_state);

    $settings = $this->settingsReader
      ->getConstraintConfig($field_config, $plugin_id);
    $plugin = $this->fieldConstraintManager
      ->createInstance($plugin_id, $settings);
    if (!$plugin instanceof ConfigurableFieldConstraintInterface) {
      throw new \LogicException(sprintf(
        'The %s plugin is not configurable.',
        $plugin_id
      ));
    }

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->getConstraintPlugin($form_state)
      ->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugin = $this->getConstraintPlugin($form_state);
    $plugin->submitConfigurationForm($form, $form_state);

    $plugin_id = $this->getConstraintPluginId($form_state);
    $settings = $plugin->getConfiguration();

    $field_config = $this->getFieldConfig($form_state);
    $this->settingsWriter
      ->setConstraintConfig($field_config, $plugin_id, $settings);
    $field_config->save();

    $this->messenger()
      ->addStatus($this->t('The constraint settings were saved.'));

    $this->setRedirectToConstraintCollection($field_config, $form_state);
  }

}
