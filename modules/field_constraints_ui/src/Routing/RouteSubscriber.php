<?php

namespace Drupal\field_constraints_ui\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\field_constraints_ui\Controller\FieldConstraintListController;
use Drupal\field_constraints_ui\FieldConfig\PermissionProviderInterface;
use Drupal\field_constraints_ui\FieldConfig\RouteProviderInterface;
use Drupal\field_constraints_ui\Form\AddConstraintForm;
use Drupal\field_constraints_ui\Form\DeleteConstraintForm;
use Drupal\field_constraints_ui\Form\EditConstraintForm;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * The routing event subscriber.
 *
 * Adds dynamic routes for field constraints UI of all the entity types.
 *
 * @see \Drupal\field_ui\Routing\RouteSubscriber
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The routes alter event to subscribe to.
   */
  protected const ROUTES_ALTER_EVENT = RoutingEvents::ALTER;

  /**
   * The listing controller class.
   */
  public const LIST_CONTROLLER = FieldConstraintListController::class;

  /**
   * The form class responsible for adding the constraint to the field.
   */
  public const ADD_CONSTRAINT_FORM = AddConstraintForm::class;

  /**
   * The form class responsible for editing the constraint of the field.
   */
  public const EDIT_CONSTRAINT_FORM = EditConstraintForm::class;

  /**
   * The form class responsible for removing the constraint from the field.
   */
  public const DELETE_CONSTRAINT_FORM = DeleteConstraintForm::class;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The permission provider.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\PermissionProviderInterface
   */
  protected $permissionProvider;

  /**
   * The route provider.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * A constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    PermissionProviderInterface $permission_provider,
    RouteProviderInterface $route_provider
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->permissionProvider = $permission_provider;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $entity_types = $this->entityTypeManager->getDefinitions();

    foreach ($entity_types as $entity_type_id => $entity_type) {
      $route_name = $entity_type->get('field_ui_base_route');
      if (!$route_name) {
        continue;
      }

      // Try to get the route from the current collection.
      $entity_route = $collection->get($route_name);
      if (!$entity_route) {
        continue;
      }

      $base_path = $entity_route->getPath();
      $constraints_path = "$base_path/fields/{field_config}/constraints";
      $permission_name = $this->permissionProvider
        ->getEntityTypePermission($entity_type_id);

      $options = $entity_route->getOptions();
      if ($bundle_entity_type = $entity_type->getBundleEntityType()) {
        $options['parameters'][$bundle_entity_type] = [
          'type' => 'entity:' . $bundle_entity_type,
        ];
      }

      $route = new Route(
        $constraints_path,
        [
          '_controller' => static::LIST_CONTROLLER . '::listing',
          '_title_callback' => static::LIST_CONTROLLER . '::title',
        ],
        [
          '_entity_access' => 'field_config.update',
          '_permission' => $permission_name,
        ],
        $options
      );
      $route_name = $this->routeProvider
        ->getCollectionRouteName($entity_type_id);
      $collection->add($route_name, $route);

      $route = new Route(
        "$constraints_path/add",
        [
          '_form' => static::ADD_CONSTRAINT_FORM,
          '_title' => 'Add constraint',
        ],
        [
          '_entity_access' => 'field_config.update',
          '_permission' => $permission_name,
        ],
        $options
      );
      $route_name = $this->routeProvider
        ->getAddFormRouteName($entity_type_id);
      $collection->add($route_name, $route);

      $route = new Route(
        "$constraints_path/edit/{plugin_id}",
        [
          '_form' => static::EDIT_CONSTRAINT_FORM,
          '_title_callback' => static::EDIT_CONSTRAINT_FORM . '::title',
        ],
        [
          '_entity_access' => 'field_config.update',
          '_permission' => $permission_name,
        ],
        $options
      );
      $route_name = $this->routeProvider
        ->getEditFormRouteName($entity_type_id);
      $collection->add($route_name, $route);

      $route = new Route(
        "$constraints_path/delete/{plugin_id}",
        [
          '_form' => static::DELETE_CONSTRAINT_FORM,
          '_title_callback' => static::DELETE_CONSTRAINT_FORM . '::title',
        ],
        [
          '_entity_access' => 'field_config.update',
          '_permission' => $permission_name,
        ],
        $options
      );
      $route_name = $this->routeProvider
        ->getRemoveFormRouteName($entity_type_id);
      $collection->add($route_name, $route);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[static::ROUTES_ALTER_EVENT] = ['onAlterRoutes', -50];
    return $events;
  }

}
