<?php

namespace Drupal\field_constraints_ui\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local task definitions for field constraints of all entity types.
 */
class LocalTaskDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route provider.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new static();

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->routeProvider = $container->get(
      'field_constraints_ui.field_config.route_provider'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!$entity_type->get('field_ui_base_route')) {
        continue;
      }

      $collection_route_name = $this->routeProvider
        ->getCollectionRouteName($entity_type_id);
      $this->derivatives["collection_{$entity_type_id}"] = [
        'route_name' => $collection_route_name,
        'title' => $this->t('Constraints'),
        'base_route' => "entity.field_config.{$entity_type_id}_field_edit_form",
      ];
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
