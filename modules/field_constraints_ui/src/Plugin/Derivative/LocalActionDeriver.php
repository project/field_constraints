<?php

namespace Drupal\field_constraints_ui\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local action definitions for field constraints of all entity types.
 */
class LocalActionDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route provider.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    $base_plugin_id
  ) {
    $instance = new static();

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->routeProvider = $container->get(
      'field_constraints_ui.field_config.route_provider'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!$entity_type->get('field_ui_base_route')) {
        continue;
      }

      $collection_route_name = $this->routeProvider
        ->getCollectionRouteName($entity_type_id);
      $add_route_name = $this->routeProvider
        ->getAddFormRouteName($entity_type_id);
      $this->derivatives["add_form_{$entity_type_id}"] = [
        'route_name' => $add_route_name,
        'title' => $this->t('Add constraint'),
        'appears_on' => [
          $collection_route_name,
        ],
      ];
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
