<?php

namespace Drupal\field_constraints_ui\Controller;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\field\FieldConfigInterface;
use Drupal\field_constraints\ConfigurableFieldConstraintInterface;
use Drupal\field_constraints\Dictionary\FieldConstraintAnnotationKeys;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles field constraints list routes.
 */
class FieldConstraintListController extends ControllerBase {

  use StringTranslationTrait,
    RedirectDestinationTrait;

  /**
   * The label key on the field constraint plugin definition.
   */
  protected const LABEL_DEFINITION_KEY = FieldConstraintAnnotationKeys::LABEL;

  /**
   * The class name key on the field constraint plugin definition.
   */
  protected const CLASS_DEFINITION_KEY = FieldConstraintAnnotationKeys::CLASS_NAME;

  /**
   * The settings reader.
   *
   * @var \Drupal\field_constraints\FieldConfig\FieldSettingsReaderInterface
   */
  protected $settingsReader;

  /**
   * The field constraints plugin manager.
   *
   * @var \Drupal\field_constraints\FieldConstraintManagerInterface
   */
  protected $fieldConstraintManager;

  /**
   * The field constraint URL builder.
   *
   * @var \Drupal\field_constraints_ui\FieldConfig\ConstraintUrlBuilderInterface
   */
  protected $constraintUrlBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var static $instance */
    $instance = parent::create($container);

    $instance->settingsReader = $container->get(
      'field_constraints.field_config.settings_reader'
    );
    $instance->fieldConstraintManager = $container->get(
      'plugin.manager.field_constraints_manager'
    );
    $instance->constraintUrlBuilder = $container->get(
      'field_constraints_ui.field_config.constraint_url_builder'
    );

    return $instance;
  }

  /**
   * Provides title callback for the constraint listing route.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config entity.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The title.
   */
  public function title(FieldConfigInterface $field_config): MarkupInterface {
    return $this->t('Constraints of the %name field', [
      '%name' => $field_config->label(),
    ]);
  }

  /**
   * Handles the constraint listing route of the field config.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   *
   * @return array
   *   The render array.
   */
  public function listing(FieldConfigInterface $field_config): array {
    $result = [];

    $result['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->title($field_config),
      '#rows' => [],
      '#empty' => $this->t('There are no constraints yet.'),
      '#cache' => [
        'contexts' => $field_config->getCacheContexts(),
        'tags' => $field_config->getCacheTags(),
      ],
    ];

    $plugin_definitions = $this->fieldConstraintManager->getDefinitions();
    $constraint_configs = $this->settingsReader
      ->getConstraintConfigs($field_config);
    foreach (array_keys($constraint_configs) as $plugin_id) {
      $plugin_definition = $plugin_definitions[$plugin_id] ?? [];
      $result['table']['#rows'][$plugin_id] = $this
        ->buildRow($field_config, $plugin_id, $plugin_definition);
    }

    return $result;
  }

  /**
   * Builds the header for the constraint listing.
   *
   * @return array
   *   A render array structure of header strings.
   */
  public function buildHeader() {
    return [
      'label' => $this->t('Constraint'),
      'name' => $this->t('Machine name'),
      'operations' => $this->t('Operations'),
    ];
  }

  /**
   * Builds a row for a constraint plugin in the listing.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   * @param array $plugin_definition
   *   The field constraint plugin definition.
   *
   * @return array
   *   A render array.
   */
  public function buildRow(
    FieldConfigInterface $field_config,
    string $plugin_id,
    array $plugin_definition
  ) {
    $row = [];
    $row['label'] = $plugin_definition[static::LABEL_DEFINITION_KEY]
      ?? $this->t('Broken/missing handler');
    $row['machine_name'] = $plugin_id;
    $row['operations']['data'] = $this
      ->buildOperations($field_config, $plugin_id, $plugin_definition);

    return $row;
  }

  /**
   * Builds a renderable list of operation links for the plugin.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   * @param array $plugin_definition
   *   The field constraint plugin definition.
   *
   * @return array
   *   A renderable array of operation links.
   */
  public function buildOperations(
    FieldConfigInterface $field_config,
    string $plugin_id,
    array $plugin_definition
  ) {
    $links = $this->getOperations($field_config, $plugin_id, $plugin_definition);

    return [
      '#type' => 'operations',
      '#links' => $links,
    ];
  }

  /**
   * Returns the list of operations for the field constraint plugin.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The field config.
   * @param string $plugin_id
   *   The field constraint plugin ID.
   * @param array $plugin_definition
   *   The field constraint plugin definition.
   *
   * @return array
   *   The list of operations.
   */
  public function getOperations(
    FieldConfigInterface $field_config,
    string $plugin_id,
    array $plugin_definition
  ) {
    $operations = [];

    $class = $plugin_definition[static::CLASS_DEFINITION_KEY] ?? NULL;
    if (is_subclass_of($class, ConfigurableFieldConstraintInterface::class)) {
      $url = $this->constraintUrlBuilder
        ->buildEditUrl($field_config, $plugin_id);

      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => $this->ensureDestination($url),
      ];
    }

    $url = $this->constraintUrlBuilder
      ->buildRemoveUrl($field_config, $plugin_id);
    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'weight' => 100,
      'url' => $this->ensureDestination($url),
    ];

    uasort($operations, SortArray::class . '::sortByWeightElement');

    return $operations;
  }

  /**
   * Ensures that a destination is present on the given URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL object to which the destination should be added.
   *
   * @return \Drupal\Core\Url
   *   The updated URL object.
   */
  protected function ensureDestination(Url $url) {
    return $url->mergeOptions(['query' => $this->getDestinationArray()]);
  }

}
